import yaml

def load(path_config="config.yaml"):
    """
        Charge un fichier de configuration
        Retourne le fichier de configuration au format dictionnaire
        Arguments:
            - path_config (str) : Chemin vers le fichier de configuration
        Retour:
            - dict : Valeur du fichier de configuration
    """
    with open(path_config, "r") as config:
        data_config = yaml.load(config)
    return data_config