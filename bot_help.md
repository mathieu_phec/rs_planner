# Aide pour l'utilisation du Bot

## Créer un événement

Pour créer un événement, on utilise la syntaxe suivante :
```
$create_event "NOM EVENEMENT" DATE EVENEMENT TAILLE DU GROUPE
Le nom de l'événement et la date sont des chaines de caractères (str)
Le nom de l'événement doit être entre "" si il contient des espaces
La date de l'événement doit être sous la forme YYYYMMDD

Par exemple, pour créer l'événement **Instance RD** le **09/03/2019** pour **8** joueurs on utilisera la syntaxe
    $create_event "Instance RD" "20190309" 8
```

## Lister les événements

Pour lister les événements a venir, on utilise la syntaxe suivante :
```
$list_events
```

## Détailler un événement

Pour détailler un événement, on utilise la syntaxe suivante :
```
$describe_event EVENT_ID

Par exemple, pour détailler l'événement 1 on utilisera la syntaxe
    $describe_event 1
```

## Rejoindre un événement

Pour rejoindre un événement, on utilise la syntaxe suivante :
```
$join_event EVENT_ID ROLE
Le role est une chaine de caractères (str)
Le role peut etre :
* DPS
* TANK
* HEAL

Par exemple, pour rejoindre l'événement **1** avec le rôle **TANK** on utilisera la syntaxe
    $join_event 1 TANK
```

## Quitter un événement

Pour quitter un événement, on utilise la syntaxe suivante :
```
$leave_event EVENT_ID
Le role est une chaine de caractères (str)

Par exemple, pour quitter l'événement **1** on utilisera la syntaxe
    $leave_event 1
```