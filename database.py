import sqlite3
import datetime

def dict_factory(cursor, row):
    """
        Change le format de retour
        list -> list(dict)
    """
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def load(path_database="db/planner.db"):
    """
        Charge la base de donnée
        Retourne un objet base de données SQLite3
        Arguments:
            - path_database (str) : Chemin vers le fichier de base de données
        Retour:
            - object : Objet representant la base de données SQLite3
    """
    conn = sqlite3.connect(path_database)

    #Create table event
    sql_create_event_table = "CREATE TABLE IF NOT EXISTS EVENT (name_event, date_event, size_event)"
    cursor = conn.cursor()
    cursor.execute(sql_create_event_table)

    #Create table player event
    sql_create_player_table = "CREATE TABLE IF NOT EXISTS PLAYER (role_player, name_player, event_id)"
    cursor = conn.cursor()
    cursor.execute(sql_create_player_table)

    #Format result to dict
    conn.row_factory = dict_factory

    return conn

def close(conn):
    """
        Ferme la connexion à la base de données
        Arguments:
            - conn (object) : Connection à la base de données
        Retour:
            - Aucun
    """
    conn.close()

def create_event(conn, event):
    """
        Créer un événement dans la base de données
        Renvoie Vrai ou Faux
        Arguments:
            - conn (object) : Connection à la base de données
            - event (dict) : Dictionnaire de l'évenement
        Retour:
            - bool : Vrai ou Faux
    """
    #Il faut formater la date (str) en datetime.date
    event_datetime = datetime.datetime.strptime(event["date_event"], "%Y%m%d") 
   
    #On injecte dans la base de données
    cursor = conn.cursor()
    sql_create_event = "INSERT INTO EVENT VALUES (:name_event, :date_event, :size_event)"
    cursor.execute(sql_create_event, event)
    conn.commit()
    return True
   
def get_events(conn, date_event):
    """
        Renvoie tous les événements d'une date donnée
        Arguments:
            - conn (object) : Connection à la base de données
            - date_event (str) : Date de l'événement
        Retour:
            - resultset : Liste des événements
    """
    #Il faut formater la date (str) en datetime.date
    event_datetime = datetime.datetime.strptime(date_event, "%Y%m%d")

    #On recherche par rapport a une date donnée
    cursor = conn.cursor()
    sql_get_events = "SELECT rowid, name_event, date_event, size_event FROM EVENT WHERE date_event>=?"
    res_events = cursor.execute(sql_get_events, (date_event,)).fetchall()

    return res_events

def get_event(conn, id_event):
    """
        Renvoie l'événement par rapport a un identifiant
        Arguments:
            - conn (object) : Connection à la base de données
            - id_event (int) : Identifiant de l'événement
        Retour:
            - resultset : Détail de l'événement
    """

    #On recherche par rapport a une date donnée
    cursor = conn.cursor()
    sql_get_event = "SELECT rowid, name_event, date_event, size_event FROM EVENT WHERE rowid=?"
    res_event = cursor.execute(sql_get_event, (id_event,)).fetchall()

    return res_event

def get_player_from_event(conn, event_id):
    """
        Renvoie la liste des joueurs pour un evenement donnée
        Arguments:
            - conn (object) : Connection à la base de données
            - event_id (id) : Identifiant de l'événement
        Retour:
            - resultset : Liste des joueurs
    """
    #On recherche par rapport a un id
    cursor = conn.cursor()
    sql_get_players = "SELECT rowid, role_player, name_player, event_id FROM PLAYER where event_id=?"
    res_players = cursor.execute(sql_get_players, (event_id,)).fetchall()

    return res_players


def add_player_to_event(conn, player):
    """
        Ajoute un joueur a un evenement
        Renvoie Vrai ou Faux
        Arguments:
            - conn (object) : Connection à la base de données
            - event (dict) : Dictionnaire de l'évenement
        Retour:
            - bool : Vrai ou Faux
    """
    #On injecte dans la base de données
    cursor = conn.cursor()
    sql_add_player = "INSERT INTO PLAYER VALUES (:role_player, :name_player, :event_id)"
    cursor.execute(sql_add_player, player)
    conn.commit()

    return True

def remove_player_to_event(conn, player):
    """
        Supprime un joueur d'un événement
        Renvoie Vrai ou Faux
        Arguments:
            - conn (object) : Connection à la base de données
            - event (dict) : Dictionnaire de l'évenement
        Retour:
            - bool : Vrai ou Faux
    """
    #On supprime dans la base de données
    cursor = conn.cursor()
    sql_remove_player = 'DELETE FROM PLAYER WHERE event_id=? and name_player=?'
    cursor.execute(sql_remove_player, (player["event_id"], player["name_player"]))
    conn.commit()

    return True

def get_events_players(conn, date_event = None, id_event = None):
    """
        Renvoie la liste des evenements avec les joueurs associés pour une date donnée ou un identifiant
        Arguments:
            - conn (object) : Connection à la base de données
            - date_event (str) : Date de l'événement, par défaut None
            - id_event (int) : Identifiant de l'événement, par défaut None
        Retour:
            - resultset : Liste des événements
    """

    #On recupere la liste des evenements
    if date_event == None:
        events = get_event(conn, id_event)
    else:
        events = get_events(conn, date_event)

    cursor = conn.cursor()

    #Pour chaque evenements, on doit recuperer la liste des joueurs
    for event in events:
        #On ajoute ...
        event["players"] = get_player_from_event(conn, event["rowid"])
    
    return events
