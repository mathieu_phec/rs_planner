import pytest
import database
from pathlib import Path

DATABASE_PATH = "tests/planner.db"

def remove_database():
    """
        Remove database file
    """
    db = Path(DATABASE_PATH)
    if db.exists():
        db.unlink()

def test_0001_load_database():
    """
        Load database
    """

    remove_database()

    conn = database.load(DATABASE_PATH)
    database.close(conn)

def test_0002_create_event():
    """
        Create event into database
    """
    
    remove_database()

    conn = database.load(DATABASE_PATH)

    event = {
        "name_event" : "TEST01", 
        "date_event" : "20190301", 
        "size_event": 8}

    assert database.create_event(conn, event) == True

def test_0003_load_event():
    """
        Create and load event
    """

    remove_database()

    conn = database.load(DATABASE_PATH)

    event = {
        "name_event" : "TEST01", 
        "date_event" : "20190301", 
        "size_event": 8}

    assert database.create_event(conn, event) == True

    #On ajoute le rowid de l'evenement
    event["rowid"] = 1

    assert database.get_events(conn, "20190301") == [event]

def test_0004_add_player_to_event():
    """
        Create a event and add player
    """

    remove_database()

    conn = database.load(DATABASE_PATH)

    event = {
        "name_event" : "TEST01", 
        "date_event" : "20190301", 
        "size_event": 8}

    assert database.create_event(conn, event) == True

    #On ajoute le rowid de l'evenement
    event["rowid"] = 1

    player_data = {
        "event_id" : 1, 
        "name_player" : "TEST", 
        "role_player" : "TANK"
    }
    
    assert database.add_player_to_event(conn, player_data) == True

    #On ajoute le rowid du joueur
    player_data["rowid"] = 1

    #On verifie
    event["players"] = [player_data]

    assert database.get_events_players(conn, date_event = "20190301") == [event]

def test_0005_add_player_and_remove_to_event():
    """
        Create a event add player and remove it
    """

    remove_database()

    conn = database.load(DATABASE_PATH)

    event = {
        "name_event" : "TEST01", 
        "date_event" : "20190301", 
        "size_event": 8}

    assert database.create_event(conn, event) == True

    #On ajoute le rowid de l'evenement
    event["rowid"] = 1

    player_data = {
        "event_id" : 1, 
        "name_player" : "TEST", 
        "role_player" : "TANK"
    }

    assert database.add_player_to_event(conn, player_data) == True

    #On ajoute le rowid du joueur
    player_data["rowid"] = 1

    #On verifie
    event["players"] = [player_data]

    assert database.get_events_players(conn, date_event = "20190301") == [event]

    #On supprime le rowid du joueur
    del player_data["rowid"]

    assert database.remove_player_to_event(conn, player_data) == True

    #On remplace le bloc player de l'event d'origine
    event["players"] = []

    assert database.get_events_players(conn, date_event = "20190301") == [event]

def test_0006_list_all_events():
    """
        List all events
    """

    remove_database()

    conn = database.load(DATABASE_PATH)

    event = {
        "name_event" : "TEST01", 
        "date_event" : "20190301", 
        "size_event": 8}

    assert database.create_event(conn, event) == True

    event2 = {
        "name_event" : "TEST02", 
        "date_event" : "20190303", 
        "size_event": 4}

    assert database.create_event(conn, event2) == True

    #no return, date is too old
    event3 = {
        "name_event" : "TEST03", 
        "date_event" : "20180303", 
        "size_event": 2}

    assert database.create_event(conn, event3) == True

    #On ajoute le rowid des evenements
    event["rowid"] = 1
    event2["rowid"] = 2
    event3["rowid"] = 3

    assert database.get_events(conn, "20190301") == [event, event2]

def test_0007_describe_event():
    """
        Create a event and add player
        Get a describe for event
    """

    remove_database()

    conn = database.load(DATABASE_PATH)

    event = {
        "name_event" : "TEST01", 
        "date_event" : "20190301", 
        "size_event": 8}

    assert database.create_event(conn, event) == True

    #On ajoute le rowid de l'evenement
    event["rowid"] = 1

    player_data = {
        "event_id" : 1, 
        "name_player" : "TEST", 
        "role_player" : "TANK"
    }
    
    assert database.add_player_to_event(conn, player_data) == True

    #On ajoute le rowid du joueur
    player_data["rowid"] = 1

    #On verifie
    event["players"] = [player_data]

    assert database.get_events_players(conn, id_event = 1) == [event]

