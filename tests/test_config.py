import pytest
import config

def test_0001_load_config():
    """
        Load config file
    """

    data_config = config.load()
    assert data_config != {}

def test_0002_test_some_values():
    """
        Load config file and check some values
    """

    data_config = config.load()
    assert data_config.get("DATABASE", None) is not None