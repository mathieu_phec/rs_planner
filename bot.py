import datetime

import discord
from discord.ext import commands

import config
import database

#Chargement de la configuration
data_config = config.load()
conn = database.load(data_config["DATABASE"]["path"])

#On charge le bot et on ajoute notre prefix
description = """RS Bot Planner version %s. Pour plus d'aide, utiliser $help_full""" %(data_config["BOT"]["version"])
bot = commands.Bot(command_prefix='$', description=description)

@bot.event
async def on_ready():
    print('Bot is ready for use')

@bot.command()
async def help_full(ctx):
    """
        Affiche l'aide complete
    """
    f = open("bot_help.md", "r", encoding="utf-8")
    raw_markdown = f.read()
    await ctx.send(raw_markdown)

@bot.command()
async def create_event(ctx, name_event, date_event, size_event):
    """
        Créer un evenement
        Par exemple : $create_event "Instance RD" "20190309" 8
    """
    event = {
        "name_event" : name_event, 
        "date_event" : date_event, 
        "size_event": size_event
    }

    database.create_event(conn, event)

    await ctx.send("L'événement **{name_event}** pour la date du **{date_event}** est maintenant disponible !".format(**event))
    await ctx.send("Utiliser la commande $list_events pour connaître son identifiant !")

@bot.command()
async def list_events(ctx):
    """
        Liste les evenements a venir
    """
    #On recupere la date du jour
    current_date = datetime.datetime.now().strftime("%Y%m%d")
    #On recupere les events
    events = database.get_events(conn, current_date)
    
    await ctx.send("Liste des événements à partir du **%s**" %(current_date))
    for event in events:
        event["nb_players"] = len(database.get_events_players(conn, id_event =event["rowid"])[0]["players"])
        await ctx.send("**{name_event} (id {rowid})** le **{date_event}** **{nb_players}/{size_event}**".format(**event))
   

@bot.command()
async def describe_event(ctx, event_id):
    """
        Détaille un evenement par son identifiant.
        Par exemple : $describe_event 1
    """
    event = database.get_events_players(conn, id_event = int(event_id))
    event = event[0]
    
    event["nb_players"] = len(event["players"])

    await ctx.send("**{name_event} (id {rowid})** le **{date_event}** **{nb_players}/{size_event}**".format(**event))
    for player in event["players"]:
        await ctx.send("* {name_player} {role_player}".format(**player))


@bot.command()
async def join_event(ctx, event_id, role_player):
    """
        Ajoute le joueur courant a un evenement avec un role donné.
        Par exemple : $join_event 1 TANK
    """

    player = {
        "event_id" : int(event_id), 
        "name_player" : ctx.message.author.name, 
        "role_player" : role_player
    }

    database.add_player_to_event(conn, player)

    await ctx.send("Vous êtes bien ajouté a l'événement **{event_id}** avec le rôle **{role_player}** !".format(**player))

@bot.command()
async def leave_event(ctx, event_id):
    """
        Retire le joueur courant d'un evenement. 
        Par exemple : $leave_event 1
    """
    
    player = {
        "event_id" : int(event_id), 
        "name_player" : ctx.message.author.name
    }
    
    database.remove_player_to_event(conn, player)

    await ctx.send("Vous êtes bien supprimé de l'événement **{event_id}** !".format(**player))

if __name__ == '__main__':   
    #On lance le bot
    bot.run(data_config["BOT"]["token"])