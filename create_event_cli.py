#Cli version
#Insert into db directly
#Usefull with crontab
#Example
# For creating TEST01 event with 8 player each days 23h30 
# 30 23 * * * python create_event_cli.py "TEST01" "`date +\%Y\%m\%d`" 8

import sys

import database
import config

#Chargement de la configuration
data_config = config.load()
conn = database.load(data_config["DATABASE"]["path"])

name_event = sys.argv[1].strip()
date_event = sys.argv[2].strip()
size_event = sys.argv[3].strip()

event = {
    "name_event" : name_event, 
    "date_event" : date_event, 
    "size_event": size_event
}

database.create_event(conn, event)