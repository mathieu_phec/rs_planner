# RS Planner

Un organiseur simple pour la guilde Rising Sun du serveur T4C Neerya

## Utilisation

Voir le fichier bot_help.md pour l'utilisation du bot

## Developpement

### Pré requis

* Python (version 3)
* Python virtualenv

* virtualenv env --no-site-packages
* Pour windows uniquement : Set-ExecutionPolicy Unrestricted -scope process
* source env/bin/activate (pour Windows .\env\Scripts\activate.ps1)
* pip install -r requirements.txt

## Pytest

* Avoir l'environnement virtualenv
* python -m pytest